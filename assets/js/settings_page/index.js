import { render } from "@wordpress/element";
import App from "./app";

render(<App />, document.getElementById("plugin"));
